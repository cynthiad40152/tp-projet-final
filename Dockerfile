FROM ubuntu:18.04 
# Make sure the package repository is up to date. 
RUN apt-get update 
RUN apt-get -y upgrade 
RUN apt-get install -y git 
# Install a basic SSH server 
RUN apt-get install -y openssh-server 
RUN sed -i 's|session    required     pam_loginuid.so|session    optional     pam_loginuid.so|g' /etc/pam.d/sshd 
RUN mkdir -p /var/run/sshd 
# Install JDK 8 (latest edition) 
RUN apt update 
RUN apt install -y openjdk-8-jdk 
# Add user jenkins to the image 
#RUN adduser --quiet jenkins 
RUN useradd stage && echo 'stage:stage' | chpasswd

ADD http://mirror.ibcp.fr/pub/apache/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz /opt/maven/

RUN tar -zxvf /opt/maven/apache-maven-3.5.4-bin.tar.gz -C /opt/maven/

# Set password for the jenkins user (you may want to alter this). 
#RUN echo "jenkins:jenkins" | chpasswd 
# Install ansible 
RUN apt-get install -y ansible 
# Standard SSH port 
EXPOSE 22 
RUN mkdir -p /home/stage/.ssh 
ADD id_rsa.pub /home/stage/.ssh/authorized_keys 
CMD ["/usr/sbin/sshd", "-D"]


